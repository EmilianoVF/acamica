const redis = require("redis");

// connect to redis
const redis_client = redis.createClient(
  process.env.REDIS_PORT || 6379,
  process.env.REDIS_HOST || "127.0.0.1"
);

redis_client.on("connect", function () {
  console.log("redis client connected");
});

module.exports = redis_client;
