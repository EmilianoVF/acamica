require("dotenv").config();
const express = require("express");
const cors = require("cors");
const session = require("express-session");
const passport = require("passport");
const swaggerUI = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerJSDocs = YAML.load("./src/utils/api.yaml");
require("./src/databaseMongo");
require("./src/databaseRedis");

const app = express();
// settings
app.set("port", process.env.PORT || 4000);
// middlewares
app.use(express.json());
app.use(cors({ origin: "*" }));
app.use(session({ secret: "secret", resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
// Documentacion swagger

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerJSDocs));
// routes
app.use("/", require("./src/routes/root"));
app.use("/common", require("./src/routes/common"));
app.use("/users", require("./src/routes/users"));
app.use("/login/", require("./src/routes/social"));
app.use("/menus", require("./src/routes/menu"));
app.use("/payMethod", require("./src/routes/payMethod"));
app.use("/pay", require("./src/routes/payment"));
app.use("/orders", require("./src/routes/orders"));
app.listen(app.get("port"), () => {
  console.log("server listening on port ", app.get("port"));
});

// Se inicia data de prueba
const startData = require("./src/utils/startAdminData");
startData.allData();

module.exports = app;
