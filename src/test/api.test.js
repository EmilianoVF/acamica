const request = require("supertest");
const app = require("../index");
const mongoose = require("mongoose");

/**
 * Testing POST new user endpoint
 */

describe("POST /api/users/newUser", () => {
  it("respond with 200 User created", (done) => {
    const data = {
      email: "test4@test.com",
      password: "test",
      username: "otro test",
      address: "test streat",
      payMethod: "dios",
    };
    request(app)
      .post("/api/users/newUser")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .expect('"User created"')
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("respond with 400 on bad request", (done) => {
    const data = {
      email: "",
      password: "",
      username: "",
      address: "",
      payMethod: "",
    };
    request(app)
      .post("/api/users/newUser")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(400)
      .expect('"User not created"')
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });
});
