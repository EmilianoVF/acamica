const mongoose = require("mongoose");

const PaySchema = new mongoose.Schema({
  payMethod: { type: String, required: true, unique: true },
});

const model = mongoose.model("PaySchema", PaySchema);

module.exports = model;
