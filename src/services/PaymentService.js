const axios = require("axios");
require("dotenv").config();
const redis_client = require("../databaseRedis");
const { promisify } = require("util");

const LPUSH_ASYNC = promisify(redis_client.lpush).bind(redis_client);

class PaymentService {
  async createPayment(orders, OrderID) {
    const url = "https://api.mercadopago.com/checkout/preferences";

    const body = {
      items: orders,
      back_urls: {
        failure: `${process.env.NGROK_URL}/pay/callback-failure`,
        pending: `${process.env.NGROK_URL}/pay/callback-pendig`,
        success: `${process.env.NGROK_URL}/pay/callback-success`,
      },
    };
    const payment = await axios.post(url, body, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.MERCADO_PAGO_ACCESO_TOKEN}`,
      },
    });
    await LPUSH_ASYNC(payment.data.id, OrderID);
    return payment.data.init_point;
  }

  async paypalPayment(orders, userInfo, OrderID) {
    const order = {
      intent: "CAPTURE",
      purchase_units: orders,
      payee: {
        email_addres: userInfo.email,
        name: userInfo.username,
        addres: { address_line_1: userInfo.address },
      },
      application_context: {
        brand_name: "Acamica Sprint 4",
        landing_page: "NO_PREFERENCE",
        user_action: "PAY_NOW",
        return_url: `${process.env.NGROK_URL}/pay/paypal/capture-order`,
        cancel_url: `${process.env.NGROK_URL}/pay/paypal/cancel-payment`,
      },
    };
    const params = new URLSearchParams();
    params.append("grant_type", "client_credentials");
    const {
      data: { access_token },
    } = await axios.post(
      "https://api-m.sandbox.paypal.com/v1/oauth2/token",
      params,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        auth: {
          username: process.env.PAYPAL_API_CLIENT,
          password: process.env.PAYPAL_API_SECRET,
        },
      }
    );
    const response = await axios.post(
      `${process.env.PAYPAL_API}/v2/checkout/orders`,
      order,
      {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      }
    );
    await LPUSH_ASYNC(response.data.id, OrderID);
    return response.data;
  }
}

module.exports = PaymentService;
