const menuModel = require("../models/menu");
const menuCtrl = {};
const redis_client = require("../databaseRedis");
const { promisify } = require("util");

const SET_ASYNC = promisify(redis_client.set).bind(redis_client);

menuCtrl.update = async () => {
  try {
    const allMenus = await menuModel.find();
    await SET_ASYNC("allMenus", JSON.stringify(allMenus));
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

menuCtrl.getAll = async (req, res) => {
  try {
    const allMenus = await menuModel.find();
    res.status(200).json(allMenus);
    await SET_ASYNC("allMenus", JSON.stringify(allMenus));
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

menuCtrl.newMenu = async (req, res) => {
  try {
    const { name, description, price } = req.body;
    const newMenu = new menuModel({
      name,
      description,
      price,
    });
    await newMenu.save();
    menuCtrl.update();
    return res.status(200).json("New Menu created");
  } catch (e) {
    res.status(400).json("The Menu was not created");
  }
};

menuCtrl.deleteMenu = async (req, res) => {
  try {
    console.log(req.params);
    const { name } = req.params;
    const deletedMenu = await menuModel.findOneAndDelete({ name: name });
    if (deletedMenu === null) return res.json({ message: "invalid menu name" });
    menuCtrl.update();
    return res
      .status(200)
      .json({ message: "The Menu was deleted", data: deletedMenu });
  } catch (e) {
    console.log(e);
    res.status(400).json("the menu was not deleted");
  }
};

menuCtrl.updateMenu = async (req, res) => {
  try {
    const { name } = req.params;
    const { new_name, price, description } = req.body;
    const menu = await menuModel.findOne({ name: name });
    if (menu === null) {
      return res.status(400).json("The Menu was NOT updated");
    } else {
      if (price) menu.price = price;
      if (new_name) menu.name = new_name;
      if (description) menu.description = description;
      const id = menu._id.toString();
      await menuModel.findByIdAndUpdate(id, {
        $set: {
          name: menu.name,
          price: menu.price,
          description: menu.description,
        },
      });
      menuCtrl.update();
      return res.status(200).json("The Menu was updated");
    }
  } catch (e) {
    console.log(e);
    res.status(400).json("The Menu was NOT updated");
  }
};

module.exports = menuCtrl;
