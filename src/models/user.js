const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  username: { type: String, require: true },
  address: { type: String, require: true },
  payMethod: { type: String, require: true },
  isAdmin: { type: Boolean, require: true },
  history: [
    {
      foodName: { type: String },
      price: { type: Number },
      qty: { type: Number },
      payMethod: { type: String },
      address: { type: String },
      status: { type: String },
      id: { type: Number },
    },
  ],
  current: [
    {
      foodName: { type: String },
      price: { type: Number },
      qty: { type: Number },
      payMethod: { type: String },
      address: { type: String },
      status: { type: String },
      id: { type: Number },
    },
  ],
});

const model = mongoose.model("UserSchema", UserSchema);

module.exports = model;
