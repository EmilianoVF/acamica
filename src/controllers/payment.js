const userModel = require("../models/user");
const { paidOrder } = require("../controllers/orders");

class PaymentController {
  constructor(subscriptionService) {
    this.subscriptionService = subscriptionService;
  }

  async getPaymentLink(req, res) {
    const { OrderID } = req.body;
    let orders = [];
    for (const order of OrderID) {
      const oneOrder = await userModel.findOne(
        { "current._id": order },
        { current: { $elemMatch: { _id: order } } }
      );
      const { foodName, price, qty, payMethod, address, status, id } =
        oneOrder.current[0];
      const meliOrder = {
        title: foodName,
        description: foodName,
        category_id: "Comida",
        quantity: qty,
        unit_price: price,
      };
      orders.push(meliOrder);
    }
    try {
      const payment = await this.subscriptionService.createPayment(
        orders,
        OrderID
      );

      return res.json(payment);
    } catch (error) {
      return res
        .status(500)
        .json({ error: true, msg: "Failed to create payment" });
    }
  }

  async getPaypalLink(req, res) {
    const { OrderID } = req.body;
    const { id } = req.userData;
    const user = await userModel.findById(id);
    let orders = [];
    for (const order of OrderID) {
      const oneOrder = await userModel.findOne(
        { "current._id": order },
        { current: { $elemMatch: { _id: order } } }
      );
      const { foodName, price, qty, payMethod, address, status, id } =
        oneOrder.current[0];
      const paypalOrder = {
        amount: { currency_code: "USD", value: (price * qty).toString() },
        description: foodName,
      };
      orders.push(paypalOrder);
    }
    const userInfo = {
      email: user.email,
      username: user.username,
      address: user.address,
      payMethod: user.payMethod,
    };
    try {
      const payment = await this.subscriptionService.paypalPayment(
        orders,
        userInfo,
        OrderID
      );
      for (const objeto of payment.links) {
        if (objeto.rel === "approve") {
          var link = objeto.href;
        }
      }
      return res.json(link);
    } catch (error) {
      return res
        .status(500)
        .json({ error: true, msg: "Failed to create payment" });
    }
  }
}

module.exports = PaymentController;

// "6276b3855c3f8d71116d9eae", "62730b0dac7d348023c21930","6276b295156a550a0e3c2f65"
// module.exports = paymentCtrl;
