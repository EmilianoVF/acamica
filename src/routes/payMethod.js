const express = require("express");
const router = express.Router();
const {
  newMethod,
  getAll,
  deleteMethod,
  updateMethod,
} = require("../controllers/payMethod");
const { loginAuth, AdminAuth } = require("../middlewares/auth");
const { payMethod } = require("../middlewares/redis");

router.get("/", payMethod, getAll);
router.post("/newMethod", [loginAuth, AdminAuth], newMethod);
router.delete("/deleteMethod/:payMethod", [loginAuth, AdminAuth], deleteMethod);
router.put("/updateMethod/:payMethod", [loginAuth, AdminAuth], updateMethod);

module.exports = router;
