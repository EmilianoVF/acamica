const rootCtrl = {};

rootCtrl.root = (req, res) => {
  res.status(200).json("/api-docs");
};

module.exports = rootCtrl;
