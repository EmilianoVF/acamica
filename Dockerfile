FROM node:17.7.1-alpine
WORKDIR /src
COPY package*.json ./
RUN npm ci
COPY . .
CMD ["npm","start"]