const express = require("express");
const passport = require("passport");
require("dotenv").config();
const router = express.Router();
const userModel = require("../models/user");
const jwt = require("jsonwebtoken");

const { isAuthenticated } = require("../middlewares/auth");

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((obj, done) => {
  done(null, obj);
});

router.get("/success-social-login", isAuthenticated, async (req, res) => {
  const provider = req.user.provider;
  if (provider === "linkedin") {
    var email = req.user.emails[0].value;
  } else {
    var email = req.user._json.email;
  }
  const user = await userModel.findOne({ email }).lean();
  if (!user) {
    return res
      .status(400)
      .json({ status: "error", error: "Invalid email/password" });
  } else {
    const token = jwt.sign(
      {
        id: user._id,
        email: user.email,
      },
      process.env.JWT_SECRET
    );

    res.send(
      `<p> Con el siguiente token :</p> 
      <br><p>${token}</p> 
      <br>loguearse en</p>  <a href="${process.env.NGROK_URL}/api-docs">swagger</a>`
    );
  }
});

module.exports = router;
