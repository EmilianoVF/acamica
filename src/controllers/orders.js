const userModel = require("../models/user");
const redis_client = require("../databaseRedis");
const { promisify } = require("util");
const axios = require("axios");

const GET_ASYNC = promisify(redis_client.get).bind(redis_client);
const LRANGE_ASYNC = promisify(redis_client.lrange).bind(redis_client);

const orderCtrl = {};

orderCtrl.newOrder = async (req, res) => {
  const { id } = req.userData;
  const { foodName, qty } = req.body;
  const user = await userModel.findById(id);
  const menu = JSON.parse(await GET_ASYNC("allMenus"));
  const food = menu.find((u) => u.name === foodName);
  let address = undefined;
  let payMethod = undefined;
  if ("address" in req.body) {
    address = req.body.address;
  } else {
    address = user.address;
  }

  if ("payMethod" in req.body) {
    payMethod = req.body.payMethod;
  } else {
    payMethod = user.payMethod;
  }

  if (food !== undefined) {
    order = {
      foodName: food.name,
      price: food.price,
      qty,
      address,
      payMethod,
      status: "Pendiente",
      id: user.current.length + 1,
    };
    user.current.push(order);
    const save = await user.save();
    return res.status(200).json({
      message: "New order created",
      current_orders: save.current,
      order_id: save.current[save.current.length - 1]._id,
    });
  } else {
    res.status(400).json("The Order was not created");
  }
};

orderCtrl.paidOrder = async (req, res, next) => {
  const { status, preference_id } = req.query;
  console.log(preference_id, status);

  if (status === "approved") {
    const ordersPaid = await LRANGE_ASYNC(preference_id, 0, 100);
    for (const order_id of ordersPaid) {
      await userModel.updateOne(
        { "current._id": order_id },
        { $set: { "current.$.status": "PAGADA" } }
      );
    }
    next();
  } else {
    return res.json({ message: "PAGO NO APROBADO" });
  }
};

orderCtrl.payPalOrder = async (req, res, next) => {
  const { token } = req.query;
  try {
    const response = await axios.post(
      `${process.env.PAYPAL_API}/v2/checkout/orders/${token}/capture`,
      {},
      {
        auth: {
          username: process.env.PAYPAL_API_CLIENT,
          password: process.env.PAYPAL_API_SECRET,
        },
      }
    );

    const { id, status } = response.data;
    if (status === "COMPLETED") {
      const ordersPaid = await LRANGE_ASYNC(id, 0, 100);
      for (const order_id of ordersPaid) {
        await userModel.updateOne(
          { "current._id": order_id },
          { $set: { "current.$.status": "PAGADA" } }
        );
      }
      next();
    } else {
      return res.json({ message: "PAGO NO APROBADO" });
    }
  } catch (error) {
    return res.status(500).json({ message: "Internal Server error" });
  }
};

orderCtrl.userConfirm = async (req, res) => {
  const { order_id } = req.body;
  try {
    await userModel.updateOne(
      { "current._id": order_id },
      { $set: { "current.$.status": "Confirmado" } }
    );

    return res.status(200).json("Orden Confirmada");
  } catch (e) {
    return res.status(400).json("Order not existing");
  }
};

orderCtrl.changeStatus = async (req, res) => {
  const { order_id, newStatus } = req.body;
  if (["Entregado", "En preparacion", "Enviado"].includes(newStatus)) {
    if (newStatus === "Entregado") {
      const order = await userModel.findOne(
        { order_id },
        { current: { $elemMatch: { _id: order_id } } }
      );
      order.current[0].status = newStatus;
      await userModel.updateOne(
        { order_id },
        { $push: { history: order.current[0] } }
      );
      await userModel.updateOne(
        { "current._id": order_id },
        { $pull: { current: { _id: order_id } } }
      );
      return res.status(200).json("Cambio realizado");
    } else {
      await userModel.updateOne(
        { "current._id": order_id },
        { $set: { "current.$.status": newStatus } }
      );
      return res.status(200).json("Cambio realizado");
    }
  } else {
    return res.status(400).json("Cambio NO realizado");
  }
};

orderCtrl.adminCancelOrder = async (req, res) => {
  const { order_id } = req.body;
  const order = await userModel.findOne(
    { order_id },
    { current: { $elemMatch: { _id: order_id } } }
  );
  if (order.current.length > 0) {
    await userModel.updateOne(
      { "current._id": order_id },
      { $set: { "current.$.status": "Cancelada by Admin" } }
    );
    order.current[0].status = "Cancelada by Admin";
    await userModel.updateOne(
      { order_id },
      { $push: { history: order.current[0] } }
    );
    await userModel.updateOne(
      { "current._id": order_id },
      { $pull: { current: { _id: order_id } } }
    );
    return res.status(200).json("order cancel");
  } else {
    return res.status(400).json("No se puede cancelar esta orden");
  }
};

orderCtrl.userCancelOrder = async (req, res) => {
  const { order_id } = req.body;
  const order = await userModel.findOne(
    { order_id },
    { current: { $elemMatch: { _id: order_id } } }
  );
  if (order.current.length > 0) {
    if (
      order.current[0].status === "Pendiente" ||
      order.current[0].status === "Confirmado"
    ) {
      await userModel.updateOne(
        { "current._id": order_id },
        { $set: { "current.$.status": "Cancelada by User" } }
      );
      order.current[0].status = "Cancelada by User";
      await userModel.updateOne(
        { order_id },
        { $push: { history: order.current[0] } }
      );
      await userModel.updateOne(
        { "current._id": order_id },
        { $pull: { current: { _id: order_id } } }
      );
      return res.status(200).json({ message: "order cancel" });
    } else {
      return res.status(400).json("No se puede cancelar esta orden");
    }
  } else {
    return res.status(400).json("No se puede cancelar esta orden");
  }
};

orderCtrl.userChangeOrder = async (req, res) => {
  const { order_id, newQty, address, payMethod } = req.body;
  const order = await userModel.findOne(
    { order_id },
    { current: { $elemMatch: { _id: order_id } } }
  );
  if (order.current.length > 0) {
    if (
      order.current[0].status === "Pendiente" ||
      order.current[0].status === "Confirmado"
    ) {
      if (newQty) order.current[0].qty = newQty;
      if (address) order.current[0].address = address;
      if (payMethod) order.current[0].payMethod = payMethod;
      await userModel.updateOne(
        { "current._id": order_id },
        {
          $set: {
            "current.$.qty": order.current[0].qty,
            "current.$.address": order.current[0].address,
            "current.$.payMethod": order.current[0].payMethod,
          },
        }
      );
      return res.json({ message: order.current[0] });
    } else {
      return res.status(400).json("tarde para cambiar");
    }
  } else {
    return res.status(400).json("tarde para cambiar");
  }
};

orderCtrl.allCurrentOrders = async (req, res) => {
  const allCurrents = await userModel.distinct("current");
  res.status(200).json(allCurrents);
};

orderCtrl.allHistoryOrders = async (req, res) => {
  const allHistory = await userModel.distinct("history");
  res.status(200).json(allHistory);
};

orderCtrl.allHistoryOrdersEmail = async (req, res) => {
  const { email } = req.params;
  const allHistory = await userModel.distinct("history", { email: email });
  res.status(200).json(allHistory);
};

orderCtrl.allCurrentOrdersEmail = async (req, res) => {
  const { email } = req.params;
  const allCurrent = await userModel.distinct("current", { email: email });
  res.status(200).json(allCurrent);
};

module.exports = orderCtrl;
