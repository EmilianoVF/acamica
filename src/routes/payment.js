const express = require("express");
const router = express.Router();

const PaymentController = require("../controllers/payment");
const PaymentService = require("../services/PaymentService");
const PaymentInstance = new PaymentController(new PaymentService());
const { paidOrder, payPalOrder } = require("../controllers/orders");
const { loginAuth } = require("../middlewares/auth");
// router.post("/Meli", payOrder);

router.post("/paypal", loginAuth, function (req, res, next) {
  PaymentInstance.getPaypalLink(req, res);
});

router.get("/paypal/capture-order", payPalOrder, (req, res) => {
  return res.redirect("/api-docs");
});

router.get("/paypal/cancel-payment", (req, res) => {
  return res.redirect("/api-docs");
});

router.post("/meli", loginAuth, function (req, res, next) {
  PaymentInstance.getPaymentLink(req, res);
});

router.get("/callback-success", paidOrder, (req, res) => {
  return res.redirect("/api-docs");
});

router.get("/callback-failure", (req, res) => {
  return res.redirect("/api-docs");
});

router.get("/callback-pending", (req, res) => {
  return res.redirect("/api-docs");
});

router.post("/notificaciones", (req, res) => {
  return res.sendStatus(200);
});

module.exports = router;
