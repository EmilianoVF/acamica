const express = require("express");
const router = express.Router();
const { getAll, newUser, newAdmin, login } = require("../controllers/users");
const { loginAuth, AdminAuth } = require("../middlewares/auth");

router.post("/login", login);

router.post("/newUser", newUser);

router.get("/", loginAuth, AdminAuth, getAll);

router.post("/newAdmin", loginAuth, AdminAuth, newAdmin);

module.exports = router;
