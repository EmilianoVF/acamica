const payModel = require("../models/payMethod");
const payCrtl = {};
const redis_client = require("../databaseRedis");
const { promisify } = require("util");

const SET_ASYNC = promisify(redis_client.set).bind(redis_client);

payCrtl.update = async () => {
  try {
    const allMethod = await payModel.find();
    await SET_ASYNC("payMethod", JSON.stringify(allMethod));
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

payCrtl.getAll = async (req, res) => {
  try {
    const allMethod = await payModel.find();
    res.json(allMethod);
    await SET_ASYNC("payMethod", JSON.stringify(allMethod));
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

payCrtl.newMethod = async (req, res) => {
  try {
    const { payMethod } = req.body;
    const newMethod = new payModel({
      payMethod,
    });
    await newMethod.save();
    payCrtl.update();
    return res.status(200).json("New pay Method created");
  } catch (e) {
    console.log(e);
    res.status(400).json("New pay Method NOT created");
  }
};

payCrtl.deleteMethod = async (req, res) => {
  try {
    const { payMethod } = req.params;
    const method = await payModel.findOne({ payMethod: payMethod });
    if (method === null) {
      return res.status(400).json("The pay method was NOT deleted");
    }

    await payModel.findOneAndDelete({
      payMethod: payMethod,
    });
    payCrtl.update();
    return res.status(200).json("The pay method was deleted");
  } catch (e) {
    console.log(e);
    return res.status(400).json("The pay method was NOT deleted");
  }
};

payCrtl.updateMethod = async (req, res) => {
  try {
    const { payMethod } = req.params;
    const { updatePayMethod } = req.body;
    const menu = await payModel.findOne({ payMethod: payMethod });
    if (menu === null) {
      return res.status(400).json("Pay method was NOT updated");
    }
    await payModel.findOneAndUpdate(
      { payMethod: payMethod },
      { payMethod: updatePayMethod }
    );
    payCrtl.update();
    return res.status(200).json("The pay method was updated");
  } catch (e) {
    console.log(e);
    return res.status(400).json("Pay method was NOT updated");
  }
};

module.exports = payCrtl;
