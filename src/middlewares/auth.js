const userModel = require("../models/user");
const jwt = require("jsonwebtoken");

function loginAuth(req, res, next) {
  if (req.isAuthenticated()) {
    req.logout();
  }
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.userData = decoded;
    next();
  } catch (error) {
    return res.status(401).json({ status: false, message: "Log in Invalid" });
  }
}

async function AdminAuth(req, res, next) {
  const { id } = req.userData;
  const user = await userModel.findById(id);
  if (user.isAdmin === true) {
    next();
  } else {
    return res
      .status(401)
      .json({ status: false, message: "Admin credentials not verify" });
  }
}

const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  console.log("USUARIO NO AUTENTICADO");
  res.redirect("/login");
};

exports.isAuthenticated = isAuthenticated;
exports.loginAuth = loginAuth;
exports.AdminAuth = AdminAuth;
