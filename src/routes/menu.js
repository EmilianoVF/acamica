const express = require("express");
const router = express.Router();
const {
  newMenu,
  getAll,
  deleteMenu,
  updateMenu,
} = require("../controllers/menu");
const { loginAuth, AdminAuth } = require("../middlewares/auth");
const { allMenusCache } = require("../middlewares/redis");

router.get("/", allMenusCache, getAll);
router.post("/newMenu", [loginAuth, AdminAuth], newMenu);
router.delete("/deleteMenu/:name", [loginAuth, AdminAuth], deleteMenu);
router.put("/updateMenu/:name", [loginAuth, AdminAuth], updateMenu);

module.exports = router;
