const express = require("express");
const passport = require("passport");
const FacebookStrategy = require("passport-facebook").Strategy;
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const LinkedinStrategy = require("passport-linkedin-oauth2").Strategy;
const Auth0Strategy = require("passport-auth0");
const url = require("url");
const querystring = require("querystring");
const util = require("util");
require("dotenv").config();
const router = express.Router();

router.get("/", (req, res) => {
  res.send(`<a href="${process.env.NGROK_URL}/login/facebook">Facebook</a>
  <br><a href="${process.env.NGROK_URL}/login/linkedin">Linkedin</a>
  <br><a href="${process.env.NGROK_URL}/login/google">Google</a>
  <br><a href="${process.env.NGROK_URL}/login/auth0">Auth0</a>`);
});

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOKCLIENTID,
      clientSecret: process.env.FACEBOOKCLIENTSECRET,
      callbackURL: `${process.env.NGROK_URL}/login/facebook/callback`,
      profileFields: ["email", "name"],
    },
    (accessToken, refreshToken, profile, done) => {
      return done(null, profile);
    }
  )
);

router.get(
  "/facebook",
  passport.authenticate("facebook", { scope: ["email"] })
);

router.get(
  "/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/common/success-social-login",
    failureRedirect: "/login",
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLECLIENTID,
      clientSecret: process.env.GOOGLECLIENTSECRET,
      callbackURL: `${process.env.NGROK_URL}/login/google/callback`,
      profileFields: ["email", "name"],
    },
    (accessToken, refreshToken, profile, done) => {
      return done(null, profile);
    }
  )
);

router.get("/google", passport.authenticate("google", { scope: ["email"] }));

router.get(
  "/google/callback",
  passport.authenticate("google", {
    successRedirect: "/common/success-social-login",
    failureRedirect: "/login",
  })
);

passport.use(
  new LinkedinStrategy(
    {
      clientID: process.env.LINKEDIN_CLIENT_ID,
      clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
      callbackURL: `${process.env.NGROK_URL}/login/linkedin/callback`,
      scope: ["r_emailaddress", "r_liteprofile"],
      state: true,
    },
    (accessToken, refreshToken, profile, done) => {
      return done(null, profile);
    }
  )
);

router.get("/linkedin", passport.authenticate("linkedin"));

router.get(
  "/linkedin/callback",
  passport.authenticate("linkedin", {
    successRedirect: "/common/success-social-login",
    failureRedirect: "/login",
  })
);

passport.use(
  new Auth0Strategy(
    {
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_CLIENT_ID,
      clientSecret: process.env.AUTH0_CLIENT_SECRET,
      callbackURL: `${process.env.NGROK_URL}/login/auth0/callback`,
    },
    function (accessToken, refreshToken, extraParams, profile, done) {
      return done(null, profile);
    }
  )
);

router.get(
  "/auth0",
  passport.authenticate("auth0", {
    scope: "openid email profile",
  }),
  function (req, res) {
    res.redirect("/");
  }
);

router.get(
  "/auth0/callback",
  passport.authenticate("auth0", {
    successRedirect: "/common/success-social-login",
    failureRedirect: "/login",
  })
);

module.exports = router;
