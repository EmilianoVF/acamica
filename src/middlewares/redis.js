const redis_client = require("../databaseRedis");
const { promisify } = require("util");

const GET_ASYNC = promisify(redis_client.get).bind(redis_client);
const HSET_ASYNC = promisify(redis_client.hset).bind(redis_client);
const HGET_ASYNC = promisify(redis_client.hget).bind(redis_client);

async function allMenusCache(req, res, next) {
  try {
    const allMenusCache = await GET_ASYNC("allMenus");
    if (allMenusCache && allMenusCache !== "[]") {
      return res.send(JSON.parse(allMenusCache));
    }
    next();
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
}

async function payMethod(req, res, next) {
  try {
    const payMethodCache = await GET_ASYNC("payMethod");
    if (payMethodCache && payMethodCache !== "[]") {
      return res.send(JSON.parse(payMethodCache));
    }
    next();
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
}

exports.allMenusCache = allMenusCache;
exports.payMethod = payMethod;
