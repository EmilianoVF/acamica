const express = require("express");
const router = express.Router();
const { loginAuth, AdminAuth } = require("../middlewares/auth");

const {
  newOrder,
  userConfirm,
  changeStatus,
  adminCancelOrder,
  userCancelOrder,
  userChangeOrder,
  allCurrentOrders,
  allHistoryOrders,
  allHistoryOrdersEmail,
  allCurrentOrdersEmail,
} = require("../controllers/orders");

router.post("/newOrder", loginAuth, newOrder);
router.put("/userConfirm", loginAuth, userConfirm);
router.put("/changeStatus", loginAuth, AdminAuth, changeStatus);
router.put("/adminCancelOrder", loginAuth, AdminAuth, adminCancelOrder);
router.put("/userCancelOrder", loginAuth, userCancelOrder);
router.put("/userChangeOrder", loginAuth, userChangeOrder);
router.get("/allCurrentOrders", loginAuth, AdminAuth, allCurrentOrders);
router.get("/allHistoryOrders", loginAuth, AdminAuth, allHistoryOrders);
router.get(
  "/allHistoryOrdersEmail/:email",
  loginAuth,
  AdminAuth,
  allHistoryOrdersEmail
);
router.get(
  "/allCurrentOrdersEmail/:email",
  loginAuth,
  AdminAuth,
  allCurrentOrdersEmail
);

module.exports = router;
