const mongoose = require("mongoose");

mongoose.connect(process.env.MONGODB_URI || URI, {
  useNewUrlParser: true,
});

const connection = mongoose.connection;
