const mongoose = require("mongoose");

const MenuSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  description: { type: String },
  price: { type: Number, require: true },
});

const model = mongoose.model("MenuSchema", MenuSchema);

module.exports = model;
