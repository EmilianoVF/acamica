const userModel = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const usersCtrl = {};

usersCtrl.getAll = async (req, res) => {
  try {
    const allUsers = await userModel.find();
    res.json(allUsers);
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

async function user(req, res, isAdmin) {
  try {
    const { email, password, username, address, payMethod } = req.body;
    if (email.length === 0 && password.length === 0)
      return res.status(400).json("User not created");
    const HashPassword = await bcrypt.hash(password, 10);
    const newUser = new userModel({
      email,
      password: HashPassword,
      username,
      address,
      payMethod,
      isAdmin,
    });
    await newUser.save();
    return res.status(200).json("User created");
  } catch (e) {
    console.log(e);
    res.status(400).json("User not created");
  }
}

usersCtrl.newUser = async (req, res) => {
  user(req, res, false);
};

usersCtrl.newAdmin = (req, res) => {
  user(req, res, true);
};

usersCtrl.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await userModel.findOne({ email }).lean();
  if (!user) {
    return res
      .status(400)
      .json({ status: "error", error: "Invalid email/password" });
  }
  if (await bcrypt.compare(password, user.password)) {
    const token = jwt.sign(
      {
        id: user._id,
        email: user.email,
      },
      process.env.JWT_SECRET
    );
    return res.status(200).json({ status: "ok", data: token });
  }
  res.status(400).json({ status: "error", error: "Invalid email/password" });
};

usersCtrl.externalLogin = async (req, res, next) => {
  const email = req.user._json.email;
  const user = await userModel.findOne({ email }).lean();
  if (!user) {
    return res
      .status(400)
      .json({ status: "error", error: "Invalid email/password" });
  } else {
    const token = jwt.sign(
      {
        id: user._id,
        email: user.email,
      },
      process.env.JWT_SECRET
    );

    next();
  }
};

module.exports = usersCtrl;
