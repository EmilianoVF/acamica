La documentación de Swagger se encuentra en

```
https://www.emilianovf.tk/api-docs
```

### Login Social

El login Social, Facebook, Google, Linkedin o Auth0 se realizan de

```
https://www.emilianovf.tk/login
```

El email correspondiente al login social debe estar registrado en la aplicacion para poder tener un login exitoso.

### Mercado Pago & Paypal

Para testear es necesario ingresar con los siguientes usuarios de Test que ya se encuentran registrados por default en la apicación

PAYPAL

```
email = comprador@acamica.com
contraseña = 5X^G%)On
```

MERCADO PAGO

```
email = test_user_29038610@testuser.com
contraseña = qatest215
```

Para simular un pago exitoso es necesario utilizar los siguientes datos en la tareta de credito en Mercado Pago

```
Numero de tarjeta = 5031 7557 3453 0604
Codigo de Seguridad = 123
Fecha de Vencimiento = 11/25
Nombre = APRO
DNI = 11111111
```

Para utilizar el metodo es necesario suministrar un OrderID, que una ves realizada la transaccion se cambiara su estado a 'PAGADA'

La estructura de la aplicación es la siguiente:

![Arquitectura](src/utils/arquitectura.png "Arquitectura")

El dominio fue obtenido en
[www.frenom.com](www.frenom.com "www.frenom.com"), los DNS y certificados SSL se encuentran en [www.cloudflare.com](www.cloudflare.com "www.cloudflare.com")

### AWS

El balanceador de carga recibe las peticiones de intenet, distribuye las peticiones a dos instancias de EC2. Como helth check cada 60 segundos envía una petición al directorio raíz de la aplicación.

Las instancias corresponden a las AMI de capa gratuita Amazon Linux 2. El puerto 80 de las instancias se encuentra unicamente abierto para el balanceador de carga. El puerto 22 de SSH esta abirto solo para el IP pública del Administrador.

Las dos instancias de EC2 se encuentran conectadas a una instancia de Redis dentro de la red de AWS. Mientrás que la base de datos Mongo DB se encuentra en MongoDB Atlas.

La aplicación corre en un Docker container con el comando `docker-compose up -d`

Cada instancia redirige las perticiones utilizando NGINX como servidor.

### Para utilizar la API

A Modo de ejemplo se encuentra pre cargados los siguientes usuarios

usuario administrador

```json
username: "batman"
password: "batman"
```

Un usuario sin permisos de administrador

```json
username: "robin"
password: "robin"
```

Un Menu

```json
name: "Caviar",
description: "Rico",
price: 999,
```

Y un metodo de pago

```json
payMethod: "paga Batman"
```
