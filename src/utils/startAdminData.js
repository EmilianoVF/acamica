const userModel = require("../models/user");
const menuModel = require("../models/menu");
const payModel = require("../models/payMethod");
const bcrypt = require("bcryptjs");
const startData = {};

startData.admin = async () => {
  const email = "batman@batman.com";
  const batman = await userModel.findOne({ email }).lean();
  if (!batman) {
    const HashPassword = await bcrypt.hash("batman", 10);
    const user = {
      email: "batman@batman.com",
      password: HashPassword,
      username: "batman",
      address: "baticueva",
      payMethod: "paga Batman",
      isAdmin: true,
    };
    const newUser = new userModel(user);
    await newUser.save();
  }
};

startData.mercadoPago = async () => {
  const email = "test_user_29038610@testuser.com";
  const batman = await userModel.findOne({ email }).lean();
  if (!batman) {
    const HashPassword = await bcrypt.hash("qatest215", 10);
    const user = {
      email: "test_user_29038610@testuser.com",
      password: HashPassword,
      username: "Comprador Mercado Pago",
      address: "baticueva",
      payMethod: "paga Batman",
      isAdmin: true,
    };
    const newUser = new userModel(user);
    await newUser.save();
  }
};

startData.payPal = async () => {
  const email = "comprador@acamica.com";
  const batman = await userModel.findOne({ email }).lean();
  if (!batman) {
    const HashPassword = await bcrypt.hash("5X^G%)On", 10);
    const user = {
      email: "comprador@acamica.com",
      password: HashPassword,
      username: "Comprador PayPal",
      address: "baticueva",
      payMethod: "paga Batman",
      isAdmin: true,
    };
    const newUser = new userModel(user);
    await newUser.save();
  }
};

startData.user = async () => {
  const email = "robin@robin.com";
  const robin = await userModel.findOne({ email }).lean();
  if (!robin) {
    const HashPassword = await bcrypt.hash("robin", 10);
    const user = {
      email: "robin@robin.com",
      password: HashPassword,
      username: "robin",
      address: "baticueva",
      payMethod: "paga Batman",
      isAdmin: true,
    };
    const newUser = new userModel(user);
    await newUser.save();
  }
};

startData.menu = async () => {
  const menu = {
    name: "Caviar",
    description: "Rico",
    price: 999,
  };
  const firstMenu = await menuModel.findOne({ name: menu.name }).lean();
  if (!firstMenu) {
    const newMenu = new menuModel(menu);
    await newMenu.save();
  }
};

startData.peyMethod = async () => {
  const payMethod = { payMethod: "paga Batman" };
  const firstPayMethod = await payModel.findOne({
    payMethod: payMethod.payMethod,
  });
  if (!firstPayMethod) {
    const newPayMethod = new payModel(payMethod);
    await newPayMethod.save();
  }
};

startData.allData = () => {
  startData.admin();
  startData.user();
  startData.mercadoPago();
  startData.payPal();
  startData.menu();
  startData.peyMethod();
};

module.exports = startData;
